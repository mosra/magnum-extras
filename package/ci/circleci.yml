version: 2.1

orbs:
  codecov: codecov/codecov@1.1.1

executors:
  ubuntu-18_04:
    docker:
    - image: ubuntu:bionic-20220427
  # Anything below 11.7 is deprecated as of June 6th 2022
  xcode-11_7:
    macos:
      xcode: 11.7.0
  emscripten:
    docker:
    # 1.39.0 is the oldest on Docker. Anything before 1.39.2 is useless as emar
    # randomly asserts: https://github.com/emscripten-core/emscripten/pull/9742
    # Then, anything before 1.39.6 is useless as well because emar has a
    # filesystem race and randomly complains that a file doesn't exist:
    # https://github.com/mosra/magnum/issues/413,
    # https://github.com/emscripten-core/emscripten/pull/10161
    - image: emscripten/emsdk:1.39.6-upstream
  android-29:
    machine:
      image: android:202102-01
  arm64:
    machine:
      image: ubuntu-2004:202101-01
    resource_class: arm.medium

commands:
  install-base-linux:
    parameters:
      extra:
        type: string
        default: ""
    steps:
    - run:
        name: Update apt and install base packages
        # Git is needed always for cloning Corrade
        command: |
          apt update
          if [[ "$CMAKE_CXX_FLAGS" == *"--coverage"* ]]; then export LCOV_PACKAGES="lcov curl"; fi
          # libidn11 needed by CMake
          apt install -y git ninja-build libidn11 $LCOV_PACKAGES << parameters.extra >>

  install-base-linux-arm64:
    parameters:
      extra:
        type: string
        default: ""
    steps:
    - run:
        name: Update apt and install base packages
        # Compared to Docker images this needs sudo
        command: |
          sudo apt update
          if [[ "$CMAKE_CXX_FLAGS" == *"--coverage"* ]]; then export LCOV_PACKAGES="lcov curl"; fi
          sudo apt install -y ninja-build gcc cmake $LCOV_PACKAGES << parameters.extra >>

  install-base-macos:
    parameters:
      extra:
        type: string
        default: ""
    steps:
    - run:
        name: Install base packages
        # As usual, homebrew takes five minutes to update and then explodes in
        # a spectacular way. How is this acceptable?!
        command: |
          if [[ "$CMAKE_CXX_FLAGS" == *"--coverage"* ]]; then export LCOV_PACKAGES="lcov"; fi
          HOMEBREW_NO_AUTO_UPDATE=1 brew install cmake ninja $LCOV_PACKAGES << parameters.extra >>

  install-base-android:
    steps:
    - run:
        name: Create Android 29 x86 AVD
        command: |
          SYSTEM_IMAGES="system-images;android-29;default;x86"
          sdkmanager "$SYSTEM_IMAGES"
          echo "no" | avdmanager --verbose create avd -n test -k "$SYSTEM_IMAGES"
    - run:
        name: Launch Android emulator
        command: |
          emulator -avd test -delay-adb -verbose -no-window -gpu swiftshader_indirect -no-snapshot -noaudio -no-boot-anim
        background: true
    - run:
        name: Update apt and install base packages
        # Compared to Docker images this needs sudo, for some reason
        command: |
          sudo apt update
          sudo apt install -y ninja-build

  install-gcc-4_8:
    steps:
    - run:
        name: Install GCC 4.8
        # For some reason, CMake needs a working C compiler, so provice CC as
        # well for the case when default gcc isn't installed.
        command: |
          apt install -y g++-4.8
          echo 'export CC=gcc-4.8' >> $BASH_ENV
          echo 'export CXX=g++-4.8' >> $BASH_ENV

  # While we want to make sure the project builds on GCC 4.8 as certain
  # embedded toolchains still use it, for Clang we just pick the version
  # available in the oldest supported distro.
  install-clang:
    steps:
    - run:
        # For some reason, CMake needs a working C compiler, so provice CC as
        # well for the case when default gcc isn't installed.
        #
        # The llvm-symbolizer link needs to be set in order to have usable
        # symbols in the output. This affects suppressions as well, meaning
        # they won't work if the symbols are not resolvable. It's not enough to
        # just `export ASAN_SYMBOLIZER_PATH=/usr/bin/llvm-symbolizer-6.0`
        # because it then complains that
        #
        #   External symbolizer path is set to '/usr/bin/llvm-symbolizer-6.0'
        #   which isn't a known symbolizer. Please set the path to the
        #   llvm-symbolizer binary or other known tool.
        #
        # Probably because because it detects tool type based on the filename?
        # Ugh.
        name: Install Clang
        command: |
          apt install -y clang
          echo 'export CC=clang' >> $BASH_ENV
          echo 'export CXX=clang++' >> $BASH_ENV
          ls -l /usr/bin/llvm-symbolizer-6.0
          ln -s /usr/bin/llvm-symbolizer{-6.0,}

  install-cmake-3_4:
    steps:
    - run:
        name: Install CMake 3.4
        command: |
          apt install -y wget
          mkdir -p $HOME/cmake && cd $HOME/cmake
          wget -nc --no-check-certificate https://cmake.org/files/v3.4/cmake-3.4.3-Linux-x86_64.tar.gz
          tar --strip-components=1 -xzf cmake-3.4.3-Linux-x86_64.tar.gz
          echo 'export PATH=$HOME/cmake/bin:$PATH' >> $BASH_ENV
          source $BASH_ENV && cmake --version | grep 3.4

  install-swiftshader-gles:
    parameters:
      build:
        type: string
    steps:
    - run:
        name: Install SwiftShader GLES
        # Zip from https://github.com/mosra/magnum-ci/tree/swiftshader and
        # self-hosted because GH Actions would make it too simple for people if
        # you could just download the artifacts directly, right? RIGHT?
        command: |
          mkdir -p $HOME/swiftshader && cd $HOME/swiftshader
          wget https://ci.magnum.graphics/swiftshader-gles-r5464.a6940c8e6e-<< parameters.build >>.zip
          unzip swiftshader-gles-r5464.a6940c8e6e-<< parameters.build >>.zip

  install-basis:
    steps:
    - run:
        name: Install Basis Universal
        command: |
          export BASIS_VERSION=v1_15_update2
          mkdir -p $HOME/basis_universal && cd $HOME/basis_universal
          wget -nc https://github.com/BinomialLLC/basis_universal/archive/$BASIS_VERSION.tar.gz
          tar --strip-components 1 -xzf $BASIS_VERSION.tar.gz

  build:
    parameters:
      script:
        type: string
    steps:
    - checkout
    - run:
        name: Build & test
        command: |
          if [ "$BUILD_STATIC" != "ON" ]; then export BUILD_STATIC=OFF; fi
          if [ "$BUILD_DEPRECATED" != "OFF" ]; then export BUILD_DEPRECATED=ON; fi
          if [ "$BUILD_APPLICATIONS" != "OFF" ]; then export BUILD_APPLICATIONS=ON; fi
          # Used by Emscripten
          if [ "$TARGET_GLES2" == "ON" ]; then export TARGET_GLES3=OFF; fi
          if [ "$TARGET_GLES2" == "OFF" ]; then export TARGET_GLES3=ON; fi
          ./package/ci/<< parameters.script >>

  lcov:
    steps:
    - run:
        name: Collect code coverage
        # Keep in sync with PKBUILD-coverage and appveyor-lcov.sh, please
        command: |
          lcov $LCOV_EXTRA_OPTS --directory . --capture --output-file coverage.info > /dev/null
          lcov $LCOV_EXTRA_OPTS --extract coverage.info "*/src/Magnum*/*" --output-file coverage.info > /dev/null
          lcov $LCOV_EXTRA_OPTS --remove coverage.info "*/MagnumExternal/*" --output-file coverage.info > /dev/null
          lcov $LCOV_EXTRA_OPTS --remove coverage.info "*/Test/*" --output-file coverage.info > /dev/null
          lcov $LCOV_EXTRA_OPTS --remove coverage.info "*/build/src/*" --output-file coverage.info > /dev/null
    - codecov/upload:
        file: coverage.info

jobs:
  linux-gl:
    executor: ubuntu-18_04
    environment:
      CMAKE_CXX_FLAGS: --coverage
      LCOV_EXTRA_OPTS: --gcov-tool /usr/bin/gcov-4.8
      CONFIGURATION: Debug
      PLATFORM_GL_API: GLX
    steps:
    - install-base-linux:
        extra: libgl1-mesa-dev libsdl2-dev
    - install-gcc-4_8
    - install-cmake-3_4
    - build:
        script: unix-desktop.sh
    - lcov

  linux-arm64:
    executor: arm64
    environment:
      CMAKE_CXX_FLAGS: --coverage
      CONFIGURATION: Debug
      PLATFORM_GL_API: GLX
    steps:
    # Not installing the old GCC 4.8 and CMake 3.4 to speed up. These are
    # tested more than enough on other jobs.
    - install-base-linux-arm64:
        extra: libgl1-mesa-dev libsdl2-dev
    - build:
        script: unix-desktop.sh
    - lcov

  linux-gles2:
    executor: ubuntu-18_04
    environment:
      CMAKE_CXX_FLAGS: --coverage
      LCOV_EXTRA_OPTS: --gcov-tool /usr/bin/gcov-4.8
      CONFIGURATION: Debug
      PLATFORM_GL_API: EGL
      # STUPID yml interprets unquoted ON as a boolean
      TARGET_GLES2: "ON"
    steps:
    - install-base-linux:
        extra: libsdl2-dev wget unzip
    - install-gcc-4_8
    - install-cmake-3_4
    - install-swiftshader-gles:
        build: ubuntu-16.04
    - build:
        script: unix-desktop-gles.sh
    - lcov

  linux-gles3:
    executor: ubuntu-18_04
    environment:
      CMAKE_CXX_FLAGS: --coverage
      LCOV_EXTRA_OPTS: --gcov-tool /usr/bin/gcov-4.8
      CONFIGURATION: Debug
      PLATFORM_GL_API: EGL
      # STUPID yml interprets unquoted OFF as a boolean
      TARGET_GLES2: "OFF"
    steps:
    - install-base-linux:
        extra: libsdl2-dev wget unzip
    - install-gcc-4_8
    - install-cmake-3_4
    - install-swiftshader-gles:
        build: ubuntu-16.04
    - build:
        script: unix-desktop-gles.sh
    - lcov

  linux-static:
    executor: ubuntu-18_04
    environment:
      # STUPID yml interprets unquoted ON as a boolean
      # https://stackoverflow.com/questions/53648244/specifying-the-string-value-yes-in-a-yaml-property
      BUILD_STATIC: "ON"
      CMAKE_CXX_FLAGS: --coverage
      LCOV_EXTRA_OPTS: --gcov-tool /usr/bin/gcov-4.8
      CONFIGURATION: Debug
      PLATFORM_GL_API: GLX
    steps:
    - install-base-linux:
        extra: libgl1-mesa-dev libsdl2-dev
    - install-gcc-4_8
    - install-cmake-3_4
    - build:
        script: unix-desktop.sh
    - lcov

  linux-nondeprecated:
    executor: ubuntu-18_04
    environment:
      # STUPID yml interprets unquoted OFF as a boolean
      BUILD_DEPRECATED: "OFF"
      CMAKE_CXX_FLAGS: -DCORRADE_NO_ASSERT
      CONFIGURATION: Release
      PLATFORM_GL_API: GLX
    steps:
    - install-base-linux:
        extra: libgl1-mesa-dev libsdl2-dev
    - install-gcc-4_8
    - install-cmake-3_4
    - build:
        script: unix-desktop.sh

  linux-sanitizers:
    executor: ubuntu-18_04
    environment:
      # STUPID yml interprets unquoted OFF as a boolean. Applications don't
      # have any automated tests, so building them for sanitizers doesn't make
      # sense.
      BUILD_APPLICATIONS: "OFF"
      CMAKE_CXX_FLAGS: -fsanitize=address
      CONFIGURATION: Debug
      PLATFORM_GL_API: GLX
    steps:
    - install-base-linux:
        extra: libgl1-mesa-dev libsdl2-dev
    - install-clang
    - install-cmake-3_4
    - build:
        script: unix-desktop.sh

  macos-gl:
    executor: xcode-11_7
    environment:
      CMAKE_CXX_FLAGS: --coverage
      CONFIGURATION: Debug
      PLATFORM_GL_API: CGL
    steps:
    - install-base-macos:
        extra: sdl2
    - build:
        script: unix-desktop.sh
    - lcov

  macos-gles3:
    executor: xcode-11_7
    environment:
      CMAKE_CXX_FLAGS: --coverage
      CONFIGURATION: Debug
      PLATFORM_GL_API: EGL
      # STUPID yml interprets unquoted OFF as a boolean
      TARGET_GLES2: "OFF"
    steps:
    - install-base-macos:
        extra: sdl2 wget
    - install-swiftshader-gles:
        build: macos-10.15
    - build:
        script: unix-desktop-gles.sh
    - lcov

  macos-static:
    executor: xcode-11_7
    environment:
      # STUPID yml interprets unquoted ON as a boolean
      BUILD_STATIC: "ON"
      CMAKE_CXX_FLAGS: --coverage
      CONFIGURATION: Debug
      PLATFORM_GL_API: CGL
    steps:
    - install-base-macos:
        extra: sdl2
    - build:
        script: unix-desktop.sh
    - lcov

  emscripten-webgl1:
    executor: emscripten
    environment:
      # STUPID yml interprets unquoted ON as a boolean
      TARGET_GLES2: "ON"
    steps:
    - install-base-linux
    - build:
        script: emscripten.sh

  emscripten-webgl2:
    executor: emscripten
    environment:
      # STUPID yml interprets unquoted OFF as a boolean
      TARGET_GLES2: "OFF"
    steps:
    - install-base-linux
    - install-basis
    - build:
        script: emscripten.sh

  android-x86-gles2:
    executor: android-29
    environment:
      # STUPID yml interprets unquoted ON as a boolean
      TARGET_GLES2: "ON"
    steps:
    - install-base-android
    - build:
        script: android-x86-gles.sh

  android-x86-gles3:
    executor: android-29
    environment:
      # STUPID yml interprets unquoted ON as a boolean
      TARGET_GLES2: "OFF"
    steps:
    - install-base-android
    - build:
        script: android-x86-gles.sh

  ios-gles3:
    executor: xcode-11_7
    environment:
      # STUPID yml interprets unquoted ON as a boolean
      TARGET_GLES2: "OFF"
      # Yep, xcodebuild is beyond stupid and just DOESN'T build in parallel by
      # default. The default medium resource class has 4 CPUs and Ninja uses
      # -j6, so do the same for xcodebuild.
      XCODE_JOBS: 6
    steps:
    - install-base-macos:
        extra: xcbeautify
    - build:
        script: ios-simulator-gles.sh

workflows:
  version: 2
  build:
    jobs:
    # While there's no actual execution or data dependency between the jobs,
    # this is done in order to reduce unnecessary credit usage. The GL and
    # non-deprecated Linux builds are taken as the main sanity checks. Only
    # if they pass, the rest of the jobs gets gradually executed, with further
    # dependencies especially for the macOS jobs that take the most credits.
    - linux-gl
    - linux-nondeprecated
    - linux-arm64:
        requires:
        - linux-gl
        - linux-nondeprecated
    # - linux-gles2:
    #     requires:
    #     - linux
    #     - linux-nondeprecated
    - linux-gles3:
        requires:
        - linux-gl
        - linux-nondeprecated
    - linux-static:
        requires:
        - linux-gl
        - linux-nondeprecated
    - linux-sanitizers:
        requires:
        - linux-gl
        - linux-nondeprecated
    - macos-gl:
        requires:
        - linux-gl
        - linux-nondeprecated
    - macos-gles3:
        requires:
        - linux-gles3
        - macos-gl
    - macos-static:
        requires:
        - linux-static
        - macos-gl
    # - emscripten-webgl1:
    #     requires:
    #     - linux-gles2
    #     - linux-static
    - emscripten-webgl2:
        requires:
        - linux-gles3
        - linux-static
    # - android-x86-gles2:
    #     requires:
    #     - linux-gles2
    #     - linux-static
    #     - linux-arm64
    - android-x86-gles3:
        requires:
        - linux-gles3
        - linux-static
        - linux-arm64
    # - ios-gles3:
    #     requires:
    #     - macos-gles3
    #     - macos-static
    #     - linux-arm64
